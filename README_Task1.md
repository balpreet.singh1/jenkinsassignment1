# JenkinsAssignment1

Jenkins Assignment: 1

-  Consider the scenario where you have a text file in your repository 'repo1' containing the details about the ninja batch xii in the tabular format like
S. No.    Name             Batch       Tool
1      Balpreet Singh     xii        Jenkins
2      Himanshi           xii       Sonarqube
We have one more repository 'repo2' which contains a shell script that would read a file and then print only the 'Name' and the 'Tool' column
We have to setup a Jenkins job which would clone the repo1 and as the part of my build I would like the repo 2 as well to be cloned and the shell script should be executed



## You have a text file in your repository 'repo1' containing the details about the ninja batch xii in the tabular format
![](screenshots/1.png)

## We have one more repository 'repo2' which contains a shell script that would read a file and then print only the 'Name' and the 'Tool' column
![](screenshots/2.png)

## A Jenkins job which would clone the repo1

![](screenshots/3.png)

## A Jenkins job which would clone the repo2


![](screenshots/4.png)


## Job trigger for  the job which will execute the shell script from repo2, once the details are cloned from repo1 using first job.
![](screenshots/5.png)

![](screenshots/6.png)

![](screenshots/7.png)

## After this build is successful an artifact will be generated which will contain only the name and tool of the ninja.

![](screenshots/11.png)

![](screenshots/12.png)

