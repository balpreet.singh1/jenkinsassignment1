# Consider a scenario where you are having around 10 jobs in your jenkins instance and all the jobs are having around 30 builds in their build history, now we would like to clean the clutter but not by manually going and removing every build but by using a more automated approach, discover if there is a way we can do this in jenkins and implement the same.

## Discard Old Builds option is used to achieve the task above.

![](screenshots/8.png)
