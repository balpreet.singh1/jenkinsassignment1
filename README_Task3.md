# Consider a scenario where you have a build history of failed and successful build, discover a way in jenkins to visualize this in a better way instead of going through the status globes everytime.


## Explored two plugins for better visualization

# BLUE OCEAN PLUGIN

![](screenshots/9.png)

# BUILD PIPELINE VIEW

![](screenshots/10.png)

